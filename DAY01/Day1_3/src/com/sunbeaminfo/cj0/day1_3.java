package com.sunbeaminfo.cj0;

//take input 
// Console
// Scanner 
// Command line argument
// DataInputStream 


// pass two command line arguments
// num1 and num2
// num1+num2 

public class day1_3 
{
	public static void main(String[] args) 
	{
		//int num1=args[0];//Type mismatch: cannot convert from String to int
		//int num2=args[1];//Type mismatch: cannot convert from String to int
		
		int num1=Integer.parseInt(args[0]);
		int num2=Integer.parseInt(args[1]);
		
		
		System.out.println("Result : "+(num1+num2));
	}
}


/*
 * public class day1_3 
{
	public static void main(String[] args) 
	{
		int num1=40;
		float f1=5.5f;
		double d=7.8d;
		int num2;
		num2=50; //allowed 
		System.out.println(" Number = "+num1+" \n Float Value "+f1+" Double "+d);
	}
}
*/

/*
public class day1_3 
{

	public static void main(String[] args) 
	{
		int num; // variable declaration
		//local variable num may not have been initialized
		System.out.println("Number = "+num);

	}
}
*/





/*
public class day1_3 {

	public static void main(String[] args) 
	{
		int num=45;
		System.out.println("Number = "+num);

	}

}

*/